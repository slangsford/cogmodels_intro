data {
  int<lower=1> n;
  int<lower=0,upper=n> k[4];
}
parameters {
  real<lower=0,upper=1> c;
  real<lower=0,upper=1> r;
  real<lower=0,upper=1> u;
} 
transformed parameters {
  simplex[4] theta;
  // MPT Category Probabilities for Word Pairs
  theta[1] = c * r;
  theta[2] = (1 - c) * u ^ 2;
  theta[3] = (1 - c) * 2 * u * (1 - u);
  theta[4] = c * (1 - r) + (1 - c) * (1 - u) ^ 2;
}
model {
  // Priors
  c ~ beta(1, 1);  // can be removed
  r ~ beta(1, 1);  // can be removed
  u ~ beta(1, 1);  // can be removed
  // Data
  k ~ multinomial(theta);
}
