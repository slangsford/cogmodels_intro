functions{
  real mpt_memory(
		int count_pairtogether,
		int count_separated,
		int count_singleton,
		int count_neither,
		
		real p_cluster,//c
		real p_retrive_cluster,//r
		real p_retrive_single){//u

    real p_pairtogether;
    real p_separated;
    real p_singleton;
    real p_neither;

    int all_counts[4];
    vector[4] all_probs;

  // MPT Category Probabilities for Word Pairs
  p_pairtogether = p_cluster * p_retrive_cluster;
  p_separated = (1 - p_cluster) * p_retrive_single ^ 2;
  p_singleton = (1 - p_cluster) * 2 * p_retrive_single * (1 - p_retrive_single);
  p_neither = p_cluster * (1 - p_retrive_cluster) + (1 - p_cluster) * (1 - p_retrive_single) ^ 2;

  all_counts[1] = count_pairtogether;
  all_counts[2] = count_separated;
  all_counts[3] = count_singleton;
  all_counts[4] = count_neither;

  all_probs[1] = p_pairtogether;
  all_probs[2] = p_separated;
  all_probs[3] = p_singleton;
  all_probs[4] = p_neither;
  
  return(
	 multinomial_lpmf(all_counts | all_probs)
	 );
  }
}

data {
  int<lower=1> n;
  int<lower=0,upper=n> k[4];
}
parameters {
  real<lower=0,upper=1> c;
  real<lower=0,upper=1> r;
  real<lower=0,upper=1> u;
} 

model {
  // uniform priors
  c ~ beta(1, 1);  // p_cluster
  r ~ beta(1, 1);  // p_retrive_cluster
  u ~ beta(1, 1);  // p_retrive_singleton
  // Data
  target += mpt_memory(k[1],k[2],k[3],k[4],c,r,u);
}
